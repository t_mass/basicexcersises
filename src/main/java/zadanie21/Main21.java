package zadanie21;
import java.util.Scanner;
public class Main21 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę całkowitą większą od 0");
        int c = scanner.nextInt();

        for (int i=0; i<c; i++){ // tyle wierszy
            for (int j = 0; j < c-i; j++) { // tyle spacji ( -i ponieważ z kazdym wierszem odejmujemy jedna spacje)
                System.out.print(" ");
            }
            for (int j = 0; j < i * 2 + 1; j++) { // tyle gwiazdek - z kazdym wierszem dochodza dwie gwiazdki (i*2)
                System.out.print("*");
            }
            System.out.println();
        }


    }
}
