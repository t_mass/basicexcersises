package mediumKolekcje;

import java.util.ArrayList;

public class MyArrayLista <T>{


    private static final int INIT_SIZE = 10;
    private Object[] items = new Object[INIT_SIZE];
    private int size = 0;

    public void add(Object e) {
        ensureCapacity();
        items[size] = e;
        size++;
    }

    private void ensureCapacity() {
        if (size < items.length) return;


        String[] copy = new String[items.length * 2];
        System.arraycopy(items, 0, copy, 0, items.length);
        items = copy;
    }

    public int size() {
        return size;
    }

    public int indexOf(Object o) {
        for (int i = 0; i < size; i++)
            if (items[i] == o) return i;
        throw new IllegalArgumentException("Element is not in list");
    }


    public Object get(int i) {
        if (i > size())
            throw new IndexOutOfBoundsException();
        return items[i];
    }
}

//    public String remove(int i) {


        //ArrayList<String> items2 = new ArrayList<String>();


