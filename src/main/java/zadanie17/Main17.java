package zadanie17;
import java.util.Scanner;

public class Main17 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj całkowitą liczbę dodatnią");
        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            double wartoscPotegi = Math.pow(2, i);

            if (wartoscPotegi < n) {
                System.out.println(wartoscPotegi);
            }else{
                break;
            }
        }
    }
}
