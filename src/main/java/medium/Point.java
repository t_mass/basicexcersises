package medium;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Point {

    private int x;
    private int y;

    public Point (int x, int y) {
        this.x = x;
        this.y = y;
    }
   public int[] getXAndY(){
        return new int[]{x,y};
    }
    public double calcDist(int x,int y){
       return sqrt(pow(this.x-x,2)+ pow(this.y-y,2));
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public double calcDist(Point p){
        return calcDist(p.getX(),p.getY());


    }

    }

