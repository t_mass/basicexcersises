package medium;
public class Car {
   //a tu nie(dobra praktyka dla stałych)
   private static final int WHEEL_AMOUNT=4;
    //static tu przeszkadza
    private String model;
    private int year;

    public Car(String model, int year) {
        this.model = model;
        this.year = year;
    }
    public String getModel() {
        return model;
    }
    public int getYear() {
        return year;
    }
    public static void main(String[] args) {
//        System.out.println(args.length);
//        System.out.println(args[0]);
//        System.out.println(args[1]);
//        System.out.println(args[2]);

        Car bmw=new Car("x1",2017);
        Car audi=new Car("a5",2015);
        System.out.println(bmw.getModel());
        System.out.println(audi.getModel());
    }
    public boolean equals(Object o){
        Car c= (Car)o;
       return model.equals(c.model)&& year==c.year;
    }

}
