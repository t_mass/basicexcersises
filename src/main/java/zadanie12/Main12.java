package zadanie12;
import java.util.Scanner;

public class Main12 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę całkowitą dodatnią");
        int liczba = scanner.nextInt();

        if (liczba >= 0) {
            for (int i = 0; i <= liczba; i++) {
                if (i % 2 == 1)
                    System.out.println(i);
            }
        } else {
            System.out.println("Podałeś nieprawidłową liczbę");
        }
        System.out.println();
        for (int i = 3; i < 101; i++) {
            if (i % 3 == 0 || i % 5 == 0) {
                System.out.println(i);
            }
        }
        //Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj min zakresu");
        int min = scanner.nextInt();

        // Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj max zakresu");
        int max = scanner.nextInt();

        for (int i = min; i < max; i++) {
            if (i % 6 == 0) {
                System.out.println(i);
            }
        }
    }

}
